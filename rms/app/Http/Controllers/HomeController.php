<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request,
    App\Job,
    App\Config;

class HomeController extends Controller
{
    /**
     * Call behavior handling for authentication
     * authentication section via middleware
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', [
            'except' => ['index', 'careers']
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $config = Config::where('name', 'like', 'url_' . '%')
            ->pluck('value', 'name')
            ->all();
        return view('home.menu', compact('config'));
    }

    public function careers(Request $request)
    {
        $config = Config::all()->pluck('value', 'name')->all();
        $requirements = explode(',', $config['application_requirements']);

        $keyword = $request->get('search');
        $perPage = 100;

        if (!empty($keyword)) {
            $jobs = Job::latest()->paginate($perPage);
        } else {
            $jobs = Job::latest()->paginate($perPage);
        }

        return view('home.careers', compact('config', 'requirements', 'jobs'));
    }
}
