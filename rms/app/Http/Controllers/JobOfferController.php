<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\JobOffer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class JobOfferController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [

    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Job Offer');
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = 'plantilla';

        if(in_array($request->status, ['plantilla', 'non-plantilla'])) {
            $status = $request->status;
        }

        $perPage = 100;
        $jobOffers = JobOffer::with([
                'applicant.job' => function ($query) {
                    $query->where('status', '=', 'plantilla');
                }
            ]
        )->paginate($perPage);


        return view('job-offer.index', [
            'jobOffers' => $jobOffers,
            'status' => $status
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $jobOffer = new JobOffer();
        $applicant = new Applicant();
        $status = 'plantilla';

        if(in_array($request->status, ['plantilla', 'non-plantilla'])) {
            $status = $request->status;
        }

        if ($request->reference) {
            $applicant = Applicant::where('reference_no', $request->reference)
                ->first();
        }

        return view('job-offer.create')->with([
            'appointment' => $jobOffer,
            'applicant' => $applicant,
            'action' => 'JobOfferController@store',
            'status' => $status
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JobOffer $jobOffer
     * @return \Illuminate\Http\Response
     */
    public function show(JobOffer $jobOffer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JobOffer $jobOffer
     * @return \Illuminate\Http\Response
     */
    public function edit(JobOffer $jobOffer)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\JobOffer $jobOffer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JobOffer $jobOffer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JobOffer $jobOffer
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobOffer $jobOffer)
    {
        //
    }
}
