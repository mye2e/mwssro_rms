<?php

namespace App\Http\Controllers;

use App\Applicant,
    App\Countries,
    Illuminate\Support\Facades\View,
    Illuminate\Http\Request;
use App\Job;

class ApplicantController extends Controller
{
    /**
     * @var array list of publication modes, "where did you find this posting"
     */
    protected $publication = [
        'agency' => 'Agency Web Site',
        'csc_bulletin' => 'CSC Bulletin of Vacant Position',
        'newspaper' => 'Newspaper',
        'others' => 'Others',
    ];

    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [
        'first_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
        'middle_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
        'last_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
        'email_address' => 'required|email'
    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Applicants');
        $this->middleware('auth', [
            'except' => ['create', 'store']
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 100;

        if (!empty($keyword)) {
            $applicants = Applicant::latest()->paginate($perPage);
        } else {
            $applicants = Applicant::latest()->paginate($perPage);
        }

        return view('applicant.index', compact('applicants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $applicant = new Applicant();
        $applicant->job_id = (null !== $request->id) ? $request->id : null;

        $countries = Countries::all('code', 'name')
            ->pluck('name', 'code')->toArray();
        $jobs = Job::all('id', 'title')
            ->pluck('title', 'id')->toArray();

        if (!empty($applicant->job_id) && empty($jobs[$applicant->job_id])) {
            abort(404, 'Forbidden not found.');
        }

        return view('applicant.create')->with([
            'action' => 'ApplicantController@store',
            'applicant' => $applicant,
            'civilStatus' => Applicant::getCivilStatus(),
            'gender' => Applicant::getGender(),
            'countries' => $countries,
            'jobs' => $jobs,
            'publication' => $this->publication

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->validationRules);

        $applicant = new Applicant;
        $applicant->fill($request->all());
        $applicant->reference_no = uniqid();
        $applicant->saveImageFileNames($request);
        $applicant->saveDocumentFileNames($request);
        $applicant->setIsFilipino($request->filipino);
        $applicant->setIsNaturalized($request->naturalized);
        $applicant->created_by = (\Auth::id()) ? \Auth::id() : 88888888;

        if ($applicant->save()) {
            $applicant->uploadImageFiles($request);
            $applicant->uploadDocumentFiles($request);
        }

        if (\Auth::check()) {
            return redirect('/applicant')->with('success', 'Successfully applied to vacant position.');
        } else {
            return redirect('/careers')->with('success', 'Successfully applied to vacant position.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function show($id, Request $request)
    {
        $applicant = Applicant::findOrFail($id);

        return view('applicant.show', [
            'applicant' => $applicant,
            'documentView' => $request->document
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $applicant = Applicant::findOrFail($id);
        $countries = Countries::all('code', 'name')
            ->pluck('name', 'code')->toArray();
        $jobs = Job::all('id', 'title')
            ->pluck('title', 'id')->toArray();

        return view('applicant.edit')->with([
            'action' => 'ApplicantController@update',
            'applicant' => $applicant,
            'civilStatus' => Applicant::getCivilStatus(),
            'gender' => Applicant::getGender(),
            'countries' => $countries,
            'jobs' => $jobs,
            'publication' => $this->publication,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->validationRules);

        $applicant = Applicant::findOrFail($id);
        $oldAttributes = $applicant->getAttributes();
        $applicant->fill($request->all());

        // update media names on db
        $applicant->saveImageFileNames($request, $oldAttributes);
        $applicant->saveDocumentFileNames($request, $oldAttributes);

        $applicant->setIsFilipino($request->filipino);
        $applicant->setIsNaturalized($request->naturalized);
        $applicant->updated_by = \Auth::id();

        if ($applicant->update()) {
            $applicant->deleteMediaFiles($request, $oldAttributes);
            $applicant->uploadImageFiles($request, $oldAttributes);
            $applicant->uploadDocumentFiles($request, $oldAttributes);
        }

        return redirect()->route('applicant.edit', ['id' => $applicant->id])->with('success',
            'Applicant post updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $applicant = Applicant::findOrFail($id);
        $applicant->deleteAllMediaFiles($applicant->getAttributes());
        Applicant::destroy($id);

        return redirect('/applicant')->with('success', 'Applicant data deleted!');
    }
}
