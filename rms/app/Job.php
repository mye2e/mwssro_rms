<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jobs';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['plantilla_item_number', 'title', 'grade', 'annual_basic_salary', 'daily_salary', 'status',
                            'description', 'education', 'experience', 'training', 'eligibility', 'requirements',
                            'duties_responsibilities', 'key_competencies', 'division', 'expires', 'publish'];

    /**
     * Relation: Job has many applicants
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function applicants()
    {
        return $this->hasMany('App\Applicant');
    }

    public function evaluations()
    {
        return $this->hasMany('App\Evaluation');
    }
}
