<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('jobs')) {
            Schema::create('jobs', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('plantilla_item_number')->nullable();
                $table->string('title');
                $table->text('description')->nullable();
                $table->text('education')->nullable();
                $table->text('experience')->nullable();
                $table->text('training')->nullable();
                $table->text('eligibility')->nullable();
                $table->text('duties_responsibilities')->nullable();
                $table->text('key_competencies')->nullable();
                $table->tinyInteger('grade')->nullable();
                $table->decimal('annual_basic_salary', 12, 5)->default(0)->nullable();
                $table->decimal('daily_salary', 12, 4)->default(0)->nullable();
                $table->string('status');
                $table->text('requirements')->nullable();
                $table->tinyInteger('division');
                $table->timestamp('expires')->nullable();
                $table->boolean('publish')->default(0);
                $table->timestamps();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
