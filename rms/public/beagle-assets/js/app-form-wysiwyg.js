var App = (function() {
  'use strict';

  App.textEditors = function(elements) {

    $(elements).summernote({
      height: 300,
    });

  };

  return App;
})(App || {});
