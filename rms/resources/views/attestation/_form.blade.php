@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
        rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/styles.css') }}" />
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'attestation-form']) !!}


<div class="form-group row">
  {{ Form::label('employment_status', 'Employment Status', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-12 col-sm-8 col-lg-4">
    {{ Form::select('employment_status', [], '', [
            'class' => 'form-control form-control-xs',
            'placeholder' => 'Employment Status',
        ])
    }}
  </div>
  <div class="offset-sm-3 col-sm-5 offset-lg-2 col-lg-3 col-form-label text-sm-left"><strong>CSC Action</strong></div>
</div>

<div class="form-group row">
  <label class="col-12 col-sm-2 col-form-label text-sm-right"> Period of Employment From </label>
  <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
      <input size="16" type="text" value="{{ date('Y-m-d',time()) }}" name="created_at"
             class="form-control form-control-sm"
             placeholder="Date Now"
      >
      <div class="input-group-append">
        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
      </div>
    </div>
  </div>
  {{ Form::label('employment_status', 'Action', ['class'=>'col-12 col-sm-5 col-lg-3 col-form-label text-sm-right']) }}
  <div class="col-12 col-sm-8 col-lg-3">
    {{ Form::select('employment_status', [], '', [
            'class' => 'form-control form-control-xs',
            'placeholder' => 'Employment Status',
        ])
    }}
  </div>
</div>

<div class="form-group row">
  <label class="col-12 col-sm-2 col-form-label text-sm-right"> To </label>
  <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
      <input size="16" type="text" value="{{ date('Y-m-d',time()) }}" name="created_at"
             class="form-control form-control-sm"
             placeholder="Date Now"
      >
      <div class="input-group-append">
        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
      </div>
    </div>
  </div>
  {{ Form::label('employment_status', 'Date of Action', ['class'=>'col-12 col-sm-5 col-lg-3 col-form-label text-sm-right']) }}
  <div class="col-12 col-sm-8 col-lg-3">
    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
      <input size="16" type="text" value="{{ date('Y-m-d',time()) }}" name="created_at"
             class="form-control form-control-sm"
             placeholder="Date Now"
      >
      <div class="input-group-append">
        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
      </div>
    </div>
  </div>
</div>

<div class="form-group row">
  {{ Form::label('appointment_nature', 'Nature of Appointment', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-12 col-sm-8 col-lg-4">
    {{ Form::select('appointment_nature', [], '', [
            'class' => 'form-control form-control-xs',
            'placeholder' => 'Nature of Appointment',
        ])
    }}
  </div>
  {{ Form::label('employment_status', 'Date of Release', ['class'=>'col-12 col-sm-5 col-lg-2 col-form-label text-sm-right']) }}
  <div class="col-12 col-sm-8 col-lg-3">
    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
      <input size="16" type="text" value="{{ date('Y-m-d',time()) }}" name="created_at"
             class="form-control form-control-sm"
             placeholder="Date Now"
      >
      <div class="input-group-append">
        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
      </div>
    </div>
  </div>
</div>

<div class="form-group row">
  <label class="col-12 col-sm-2 col-form-label text-sm-right"> Date of Issuance </label>
  <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
      <input size="16" type="text" value="{{ date('Y-m-d',time()) }}" name="created_at"
             class="form-control form-control-sm"
             placeholder="Date Now"
      >
      <div class="input-group-append">
        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-12 col-sm-4 col-form-label text-sm-right"><strong>PUBLICATION</strong></div>
  <div class="col-12 offset-sm-3 col-sm-5 col-lg-3 col-form-label text-sm-right">Agency Receiving Offer</div>
</div>

<div class="form-group row">
  <label class="col-12 col-sm-2 col-form-label text-sm-right"> Publication Date From </label>
  <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
      <input size="16" type="text" value="{{ date('Y-m-d',time()) }}" name="created_at"
             class="form-control form-control-sm"
             placeholder="Date Now"
      >
      <div class="input-group-append">
        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
      </div>
    </div>
  </div>
  <div class="offset-sm-3 col-sm-8 offset-lg-3 col-lg-3">
    <input size="16" type="text" value="" name="created_at"
           class="form-control form-control-sm"
           placeholder="Date Now"
    >
  </div>
</div>

<div class="form-group row">
  <label class="col-12 col-sm-2 col-form-label text-sm-right"> To </label>
  <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
      <input size="16" type="text" value="{{ date('Y-m-d',time()) }}" name="created_at"
             class="form-control form-control-sm"
             placeholder="Date Now"
      >
      <div class="input-group-append">
        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
      </div>
    </div>
  </div>
</div>

<div class="form-group row">
  {{ Form::label('publication_mode', 'Mode of Publication', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-12 col-sm-8 col-lg-3">
    {{ Form::select('publication_mode', [
        'agency_website'=>'Agency Website',
        'csc_bulletin'=>'CSC Bulletin of Vacant Positions',
        'newspaper'=>'Newspaper',
      ], '', [
            'class' => 'form-control form-control-xs',
            'placeholder' => 'Mode of Publication',
        ])
    }}
  </div>
</div>

<div class="form-group row text-right">
  <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
    {{ Form::submit('Save', ['id' => 'attestation-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>

{!! Form::close() !!}

@section('scripts')
  <!-- JS Libraries -->
  <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
          type="text/javascript"></script>
  <script>
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.formElements();
    });
  </script>
@endsection
